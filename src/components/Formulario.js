import React, { useState } from 'react';
import Error from './Error';
import shortid from 'shortid';
import PropTypes from 'prop-types';

const Formulario = ({guardarGasto, guardarCrearGasto}) => {

        const [nombre, guardarNombre] = useState('');
        const [cantidad, guardarCantidad] = useState(0);
        const [error, actualizarError] = useState(false);
        
        //Funcion para cuando el usuario ingresa el gasto
        const agregarGasto = e => {
                e.preventDefault();
        //validar
                if(cantidad<1 || isNaN(cantidad) || nombre.trim()==='' ){
                        actualizarError(true);
                        return;
                }
                actualizarError(false);
        //construir el gasto
        const gasto = {
                nombre,
                cantidad,
                id: shortid.generate()
        }
        

        //pasar el gasto al componente principal
        guardarGasto(gasto);
        guardarCrearGasto(true);


        //Resetear el formulario
        guardarNombre('');
        guardarCantidad(0)


        }
       



    return ( 
       
        <form
        onSubmit={agregarGasto}>
             <h2> Agrega tus gastos</h2>

             {error ?
              <Error mensaje="El campo debe estar lleno o la cantidad debe ser un valor positivo"/> : null}
             <div className="campo">

             <label>Nombre del gasto</label>
            <input type="text"
                    className="u-full-width"
                    placeholder="Ej. Combustible"
                    value={nombre}
                    onChange={e => guardarNombre(e.target.value)}/>

            <label>Cantidad del gasto</label>
            <input type="number"
                    className="u-full-width"
                    placeholder="Digite la cantidad del gasto"
                    value={cantidad}
                    onChange={e => guardarCantidad(parseInt(e.target.value))}/>

                     <input type="submit"
                    className="button-primary u-full-width"
                    value="Agregar gasto"
                    />



             </div>



        </form>
     );
}

Formulario.propTypes ={
        guardarGasto:PropTypes.func.isRequired,
        guardarCrearGasto: PropTypes.func.isRequired
    }


 
export default Formulario;
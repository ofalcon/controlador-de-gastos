import React, { Fragment, useState } from 'react';
import Error from './Error';
import PropTypes from 'prop-types';



const Pregunta = ({guardarPresupuesto, guardarRestante, actualizarPregunta}) => {

    //State de la pregunta
    const [cantidad, guardarCantidad] = useState(0);
    //State del error
    const [error, guardarError] = useState(false);

    //Leer lo que el usuario ingresa por pantalla
    const definirPresupuesto = e => {
        guardarCantidad(parseInt(e.target.value));
    }
    

    //metodo agregar presupuesto
    const agregarPresupuesto = e => {
        e.preventDefault();

        //Primero, validar.
        if(cantidad < 1 || isNaN(cantidad)){
            guardarError(true);
           
            return;
        }

        //Si pasa validacion, entonces:
        guardarError(false)
        guardarPresupuesto(cantidad);
        guardarRestante(cantidad);
        actualizarPregunta(false);

    }
    return ( 
        <Fragment>
            <h2> Digite su presupuesto</h2>
                { error ? <Error
                mensaje="Se debe ingresar un valor positivo"/> : null}
            <form
            onSubmit={agregarPresupuesto}>
                <input
                 type="number"
                 className="u-full-width"
                 placeholder="Ingrese aqui su presupuesto"
                 onChange={definirPresupuesto}
                 />

                 <input
                 type="submit"
                 className="button-primary u-full-width"
                 value="Definir Presupuesto"
                 />

            </form>
        </Fragment>

     );
}
Pregunta.propTypes ={
    guardarPresupuesto:PropTypes.func.isRequired,
    guardarRestante: PropTypes.func.isRequired,
    actualizarPregunta: PropTypes.func.isRequired
}
 
export default Pregunta;